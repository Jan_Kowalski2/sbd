CREATE TRIGGER preventOrder
ON S18033.Product_Order
AFTER INSERT
BEGIN
    DECLARE @v_stock INT;
    SELECT @v_stock = stock FROM product WHERE product_id = :new.product_id;
    IF :new.amount > @v_stock
    BEGIN

    END;

    IF EXISTS (SELECT 1 FROM INSERTED) 
    BEGIN
        DECLARE order_cursor CURSOR FOR
        SELECT product_id, product_order_id, amount FROM INSERTED;
        DECLARE @product_id INT, @product_order_id INT, @amount INT;
        FETCH NEXT FROM order_cursor INTO @product_id, @product_order_id, @amount;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE @v_stock INT;
            SELECT @v_stock = stock FROM product WHERE product_id = @product_id;
            IF @v_stock > @amount 
            BEGIN
                DELETE FROM Product_Order WHERE product_id = @product_id;
                PRINT('Ilosc zamowionych sztuk przekracza stan magazynowy.');
            END;
            FETCH NEXT FROM order_cursor INTO @product_id, @Empno;
        END;
        CLOSE order_cursor;
        DEALLOCATE order_cursor;
    END;
END;

INSERT INTO Order_status VALUES (1, 'Zamowiony');
INSERT INTO Single_Order VALUES (1,1,1,'Zwykle zamowienie', TO_DATE('2003/07/09', 'yyyy/mm/dd'), null);