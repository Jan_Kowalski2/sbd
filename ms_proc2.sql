CREATE PROCEDURE makeDiscountForCategory 
@p_discount INT, @p_category_id INT
AS
BEGIN
    DECLARE product_cursor CURSOR FOR SELECT product_id, category_id, price FROM product;
    DECLARE @v_products_count INT;
    SELECT @v_products_count = COUNT(1) FROM product WHERE category_id = @p_category_id;
    IF (@v_products_count = 0)
    BEGIN
        PRINT('Brak produktow w kategorii');
    END
    ELSE
    BEGIN
        DECLARE @product_id INT, @category_id INT, @price INT;
		OPEN product_cursor;
        FETCH NEXT FROM product_cursor INTO @product_id, @category_id, @price;
        WHILE @@FETCH_STATUS = 0 
        BEGIN
            IF @category_id = @p_category_id
                UPDATE Product SET price = @price*@p_discount
                WHERE product_id = @product_id;
            END
			FETCH NEXT FROM product_cursor INTO @product_id, @category_id, @price;
        END;

        CLOSE product_cursor;
        DEALLOCATE product_cursor;
    END

INSERT INTO Category VALUES (1, 'Komputery', 'Komputery stacjonarne');
INSERT INTO Category VALUES (2, 'Telefony', 'Telefony komorkowe');
INSERT INTO Product_Type VALUES (1, 'Przedmiot fizyczny');
INSERT INTO Product VALUES (1, 1, 1, 'Razer', 1000, 20, 15, '5x5x5', 'Super najlepszy komputer');
INSERT INTO Product VALUES (2, 1, 1, 'Ryzen', 2000, 20, 5, '5x4x5', 'Super najlepszy komputer od AMD');
INSERT INTO Product VALUES (3, 2, 1, 'Hulawei', 500, 20, 5, '5x1x5', 'Super najlepszy telefonik');

SELECT * FROM PRODUCT WHERE category_id = 2;

EXEC [S18033][makeDiscountForCategory]
    @p_discount = 0.9,
    @p_category_id = 2;

SELECT * FROM PRODUCT WHERE category_id = 2;