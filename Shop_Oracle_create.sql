-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-06-11 21:23:08.127

-- tables
-- Table: Category
CREATE TABLE Category (
    category_id int  NOT NULL,
    name varchar2(200)  NOT NULL,
    description varchar2(500)  NULL,
    CONSTRAINT Category_pk PRIMARY KEY (category_id)
) ;

-- Table: Employee
CREATE TABLE Employee (
    entity_person_id int  NOT NULL,
    employee_role_id int  NOT NULL,
    salary int  NOT NULL,
    CONSTRAINT Employee_pk PRIMARY KEY (entity_person_id)
) ;

-- Table: Employee_role
CREATE TABLE Employee_role (
    employee_role_id int  NOT NULL,
    name varchar2(30)  NOT NULL,
    CONSTRAINT Employee_role_pk PRIMARY KEY (employee_role_id)
) ;

-- Table: Entity_Person
CREATE TABLE Entity_Person (
    entity_person_id int  NOT NULL,
    first_name varchar2(100)  NOT NULL,
    last_name varchar2(100)  NOT NULL,
    email varchar2(100)  NOT NULL,
    hashed_password varchar2(30)  NOT NULL,
    address varchar2(300)  NOT NULL,
    payment_address varchar2(300)  NULL,
    CONSTRAINT Entity_Person_pk PRIMARY KEY (entity_person_id)
) ;

-- Table: Order_status
CREATE TABLE Order_status (
    order_status_id int  NOT NULL,
    name varchar2(30)  NOT NULL,
    CONSTRAINT Order_status_pk PRIMARY KEY (order_status_id)
) ;

-- Table: Product
CREATE TABLE Product (
    product_id int  NOT NULL,
    category_id int  NOT NULL,
    product_type_id int  NOT NULL,
    name varchar2(200)  NOT NULL,
    price number(7,0)  NOT NULL,
    weight number(5,0)  NOT NULL,
    stock number(10,0)  NOT NULL,
    dimensions varchar2(100)  NULL,
    description varchar2(500)  NULL,
    CONSTRAINT Product_pk PRIMARY KEY (product_id)
) ;

-- Table: Product_Order
CREATE TABLE Product_Order (
    product_order_id int  NOT NULL,
    product_id int  NOT NULL,
    order_id int  NOT NULL,
    amount int  NOT NULL,
    CONSTRAINT Product_Order_pk PRIMARY KEY (product_order_id)
) ;

-- Table: Product_type
CREATE TABLE Product_type (
    product_type_id int  NOT NULL,
    name varchar2(30)  NOT NULL,
    CONSTRAINT Product_type_pk PRIMARY KEY (product_type_id)
) ;

-- Table: Reclamation
CREATE TABLE Reclamation (
    reclamation_id int  NOT NULL,
    order_id int  NOT NULL,
    description varchar2(500)  NOT NULL,
    CONSTRAINT Reclamation_pk PRIMARY KEY (reclamation_id)
) ;

-- Table: Single_Order
CREATE TABLE Single_Order (
    order_id int  NOT NULL,
    order_status_id int  NOT NULL,
    entity_person_id int  NOT NULL,
    description varchar2(300)  NOT NULL,
    date_of_buy date  NOT NULL,
    address varchar2(300)  NULL,
    CONSTRAINT Single_Order_pk PRIMARY KEY (order_id)
) ;

-- foreign keys
-- Reference: Employee_Employee_role (table: Employee)
ALTER TABLE Employee ADD CONSTRAINT Employee_Employee_role
    FOREIGN KEY (employee_role_id)
    REFERENCES Employee_role (employee_role_id);

-- Reference: Employee_Person (table: Employee)
ALTER TABLE Employee ADD CONSTRAINT Employee_Person
    FOREIGN KEY (entity_person_id)
    REFERENCES Entity_Person (entity_person_id);

-- Reference: Order_Order_status (table: Single_Order)
ALTER TABLE Single_Order ADD CONSTRAINT Order_Order_status
    FOREIGN KEY (order_status_id)
    REFERENCES Order_status (order_status_id);

-- Reference: Order_Person (table: Single_Order)
ALTER TABLE Single_Order ADD CONSTRAINT Order_Person
    FOREIGN KEY (entity_person_id)
    REFERENCES Entity_Person (entity_person_id);

-- Reference: Product_Category (table: Product)
ALTER TABLE Product ADD CONSTRAINT Product_Category
    FOREIGN KEY (category_id)
    REFERENCES Category (category_id);

-- Reference: Product_Order_Order (table: Product_Order)
ALTER TABLE Product_Order ADD CONSTRAINT Product_Order_Order
    FOREIGN KEY (order_id)
    REFERENCES Single_Order (order_id);

-- Reference: Product_Order_Product (table: Product_Order)
ALTER TABLE Product_Order ADD CONSTRAINT Product_Order_Product
    FOREIGN KEY (product_id)
    REFERENCES Product (product_id);

-- Reference: Product_Product_type (table: Product)
ALTER TABLE Product ADD CONSTRAINT Product_Product_type
    FOREIGN KEY (product_type_id)
    REFERENCES Product_type (product_type_id);

-- Reference: Reclamation_Order (table: Reclamation)
ALTER TABLE Reclamation ADD CONSTRAINT Reclamation_Order
    FOREIGN KEY (order_id)
    REFERENCES Single_Order (order_id);

-- End of file.

