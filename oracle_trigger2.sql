create or replace TRIGGER forbidSalary
BEFORE INSERT OR UPDATE OR DELETE ON Employee
FOR EACH ROW
BEGIN
    IF DELETING AND :old.salary > 0 THEN
        RAISE_APPLICATION_ERROR(-20005, 'Nie można usuwać!');
    END IF;
END;