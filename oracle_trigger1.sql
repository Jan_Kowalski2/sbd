SET SERVEROUTPUT ON;
CREATE OR REPLACE TRIGGER preventOrder
BEFORE INSERT ON Product_Order
FOR EACH ROW
DECLARE 
    v_stock INT;
BEGIN
    SELECT stock INTO v_stock FROM product WHERE product_id = :new.product_id;
    IF :new.amount > v_stock THEN
        RAISE_APPLICATION_ERROR(-20004, 'Ilosc zamowionych sztuk przekracza stan magazynowy.');
    END If;
END;

INSERT INTO Order_status VALUES (1, 'Zamowiony');
INSERT INTO Single_Order VALUES (1,1,1,'Zwykle zamowienie', TO_DATE('2003/07/09', 'yyyy/mm/dd'), null);