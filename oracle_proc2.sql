SET SERVEROUTPUT ON;

create or replace PROCEDURE makeDiscountForCategory 
(
 p_discount INT, p_category_id INT
) IS
    CURSOR product_current IS 
    SELECT product_id, category_id, price FROM product;
    product_record product_current%rowtype;
    v_products_count INT := 0;
BEGIN
    SELECT COUNT(1) INTO v_products_count FROM product WHERE category_id = p_category_id;
    IF (v_products_count = 0) THEN
        RAISE_APPLICATION_ERROR(-20001,'Brak produktow w kategorii');
    ELSE
        OPEN product_current;
        LOOP
            FETCH product_current INTO product_record;
            
            IF product_record.category_id = p_category_id THEN
                UPDATE Product SET price = product_record.price*p_discount
                WHERE product_id = product_record.product_id;
            END IF;
            EXIT WHEN product_current%notfound;
        END LOOP;
    END IF;
END;

INSERT INTO Category VALUES (1, 'Komputery', 'Komputery stacjonarne');
INSERT INTO Category VALUES (2, 'Telefony', 'Telefony komorkowe');
INSERT INTO Product_Type VALUES (1, 'Przedmiot fizyczny');
INSERT INTO Product VALUES (1, 1, 1, 'Razer', 1000, 20, 15, '5x5x5', 'Super najlepszy komputer');
INSERT INTO Product VALUES (2, 1, 1, 'Ryzen', 2000, 20, 5, '5x4x5', 'Super najlepszy komputer od AMD');
INSERT INTO Product VALUES (3, 2, 1, 'Hulawei', 500, 20, 5, '5x1x5', 'Super najlepszy telefonik');

SELECT * FROM PRODUCT WHERE category_id = 2;
SET SERVEROUTPUT ON;
CALL makeDiscountForCategory(0.9, 2);
SELECT * FROM PRODUCT WHERE category_id = 2;