INSERT INTO Entity_Person VALUES (1, 'Jan', 'Kowal', 'jan@kowal.com', '123', '12 Kowalewo', null);
INSERT INTO Entity_Person VALUES (2, 'Janina', 'Kowalek', 'jan1@kowal.com', '324', '12 Kowalewo', null);

INSERT INTO Employee VALUES(1, 1, 1400);
INSERT INTO Employee VALUES(2, 1, 1600);

create PROCEDURE addEmployee 
@firstname VARCHAR(100), @lastname VARCHAR(100), 
@email VARCHAR(100), @pass VARCHAR(30), @addr VARCHAR(100), 
@role_id INT
AS
BEGIN
    DECLARE @v_avgsalary INT;
    DECLARE @v_id INT;
    DECLARE @v_role_id INT;

    SELECT @v_role_id = COUNT(*) FROM Employee_role WHERE employee_role_id = @role_id;
    SELECT @v_avgsalary = AVG(salary) FROM Employee WHERE employee_role_id = @role_id;
    IF (@v_role_id = 0) 
    BEGIN
        PRINT('Brak takiej roli!');
    END;
    ELSE
    BEGIN    
        SELECT @v_id = ISNULL(MAX(entity_person_id), 0) FROM Entity_Person; 
        INSERT INTO Entity_Person VALUES ((@v_id+1), @firstname, @lastname, @email, @pass, @addr, null);
        INSERT INTO Employee VALUES ((@v_id+1), @role_id, ISNULL(@v_avgsalary, 0));
    END;
END


EXEC [S18033][addEmployee]
    @firstname = 'Piotr',
    @lastname = 'Kowalek',
    @email = 'piotr@kowalek.pl', 
    @pass = '121', 
    @addr = 'Piotrowska 4, Kowalna', 
    @role_id = 2;

EXEC [S18033][addEmployee]
    @firstname = 'Jan',
    @lastname = 'Kowal', 
    @email = 'jan@kolalek.pl', 
    @pass = '121', 
    @addr = 'Piotrowska 3, Kowalna', 
    @role_id = 1;