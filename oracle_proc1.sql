SET SERVEROUTPUT ON;

INSERT INTO Entity_Person VALUES (1, 'Jan', 'Kowal', 'jan@kowal.com', '123', '12 Kowalewo', null);
INSERT INTO Entity_Person VALUES (2, 'Janina', 'Kowalek', 'jan1@kowal.com', '324', '12 Kowalewo', null);

INSERT INTO Employee VALUES(1, 1, 1400);
INSERT INTO Employee VALUES(2, 1, 1600);

create or replace PROCEDURE addEmployee 
(
 p_firstname VARCHAR2, p_lastname VARCHAR2, 
 p_email VARCHAR2, p_pass VARCHAR2, p_addr VARCHAR2, 
 p_role_id INT
) IS
    v_avgsalary INT := 0;
    v_id INT := 0;
    v_role_id INT := 0;
BEGIN
    SELECT COUNT(1) INTO v_role_id FROM Employee_role WHERE employee_role_id = p_role_id;
    SELECT AVG(salary) INTO v_avgsalary FROM Employee WHERE employee_role_id = p_role_id;
    IF (v_role_id = 0) THEN
        RAISE_APPLICATION_ERROR(-20001,'Brak takiej roli!');
    ELSE    
        SELECT NVL(MAX(entity_person_id), 0) INTO v_id FROM Entity_Person; 
        INSERT INTO Entity_Person VALUES ((v_id+1), p_firstname, p_lastname, p_email, p_pass, p_addr, null);
        INSERT INTO Employee VALUES ((v_id+1), p_role_id, v_avgsalary);
    END IF;
END;

CALL addEmployee('Piotr', 'Kowalek', 'piotr@kowalek.pl', '121', 'Piotrowska 4, Kowalna', 2);
CALL addEmployee('Piotr', 'Kowalek', 'piotr@kowalek.pl', '121', 'Piotrowska 4, Kowalna', 1);