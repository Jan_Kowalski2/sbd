-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-06-11 21:44:04.202

-- foreign keys
ALTER TABLE Employee DROP CONSTRAINT Employee_Employee_role;

ALTER TABLE Employee DROP CONSTRAINT Employee_Entity_Person;

ALTER TABLE Single_Order DROP CONSTRAINT Order_Order_status;

ALTER TABLE Single_Order DROP CONSTRAINT Order_Person;

ALTER TABLE Product DROP CONSTRAINT Product_Category;

ALTER TABLE Product_Order DROP CONSTRAINT Product_Order_Order;

ALTER TABLE Product_Order DROP CONSTRAINT Product_Order_Product;

ALTER TABLE Product DROP CONSTRAINT Product_Product_type;

ALTER TABLE Reclamation DROP CONSTRAINT Reclamation_Order;

-- tables
DROP TABLE Category;

DROP TABLE Employee;

DROP TABLE Employee_role;

DROP TABLE Entity_Person;

DROP TABLE Order_status;

DROP TABLE Product;

DROP TABLE Product_Order;

DROP TABLE Product_type;

DROP TABLE Reclamation;

DROP TABLE Single_Order;

-- End of file.

